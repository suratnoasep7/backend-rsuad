<!DOCTYPE html>
<html>
<head>
	<title>Apps - Dashboard</title>

	<!-- Javascript External -->
	<script src="<?php echo base_url() .'assets/external/js/jquery.js' ?>"></script>
	<script src="<?php echo base_url() .'assets/external/js/bootstrap.min.js' ?>"></script>
	<script src="<?php echo base_url() .'assets/external/js/highcharts.js' ?>"></script>
	<script src="<?php echo base_url() .'assets/external/js/highcharts-exporting.js' ?>"></script>
	<script src="<?php echo base_url() .'assets/external/js/highcharts-export-data.js' ?>"></script>
	<script src="<?php echo base_url() .'assets/external/js/highcharts-accessibility.js' ?>"></script>

	<!-- Javascript Internal -->
	<script src="<?php echo base_url() .'assets/internal/js/button_action.js'?>"></script>
	<script src="<?php echo base_url() .'assets/internal/js/modal.js' ?>"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() .'assets/external/css/bootstrap.min.css'?>">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() .'assets/internal/css/form.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() .'assets/internal/css/content.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() .'assets/internal/css/button_action.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() .'assets/internal/css/modal.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() .'assets/internal/css/loading.css' ?>">
</head>
<body class="dashboard-body">
	<div class="loading">
		<img src="<?php echo base_url() ?>assets/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-base">
			Admin Unit IT
		</div>
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init">DF</label>
				<img src="image/profile_photo/profile.png" alt="">
			</div>
			<label class="account-name">Pevita Munaf / Admin IT</label>
		</div>
		<a href="" class="btn btn-danger rounded pull-right">close</a>
	</div>