<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col col-menu pad-sm">
	<div class="menu-logo">
		<img src="<?php echo base_url() ?>assets/image/asset/app-logo.png" alt="">
	</div>
	<div class="menu-button">
		<a href="<?php echo base_url('master/master_pengguna') ?>" class="menu-item active">
			<div class="menu-icon">
				<img src="<?php echo base_url() ?>assets/image/asset/users.png" alt="">
			</div>
			<div class="menu-text">
				<div class="menu-title">PENGGUNA</div>
				<div class="menu-desc">Pengguna aplikasi dan hak akes terhadap aplikasi</div>
			</div>
		</a>
	</div>
</div>