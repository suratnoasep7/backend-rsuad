<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col col-menu pad-sm">
	<div class="menu-logo">
		<img src="<?php echo base_url() ?>assets/image/asset/app-logo.png" alt="">
	</div>
	<div class="menu-button">
		<a href="<?php echo base_url('aplikasi/dashboard') ?>" class="menu-item">
			<div class="menu-icon">
				<img src="<?php echo base_url() ?>assets/image/asset/web.png" alt="">
			</div>
			<div class="menu-text">
				<div class="menu-title">DASHBOARD</div>
				<div class="menu-desc">Merupakan preview dari aktivitas pelayanan unit IT</div>
			</div>
		</a>
	</div>
	<div class="menu-button">
		<a href="<?php echo base_url('aplikasi/laporan_aktifitas') ?>" class="menu-item">
			<div class="menu-icon">
				<img src="<?php echo base_url() ?>assets/image/asset/report.png" alt="">
			</div>
			<div class="menu-text">
				<div class="menu-title">Laporan Aktivitas</div>
				<div class="menu-desc">Laporan aktivitas penyelesaian permintaan perbaikan & maintenance</div>
			</div>
		</a>
	</div>
</div>