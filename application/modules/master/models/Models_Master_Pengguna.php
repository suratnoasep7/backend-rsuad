<?php

class Models_Master_Pengguna extends CI_Model
{ 
	
	function saveUserAccount()
	{
		
		$arrdata = array(
			'unit_id' 				=> $this->input->post('unit_id'),
			'id_account_register' 	=> $this->input->post('id_account_register'),
			'name' 					=> $this->input->post('name'),
			'email' 				=> $this->input->post('email'),
			'phone' 				=> $this->input->post('phone'),
			'status' 				=> $this->input->post('status'),
			'user_ent' 				=> $this->input->post('user_ent'),
			'date_ent' 				=> $this->input->post('date_ent')		
		);

		$this->db->trans_begin();
		$this->db->insert('user_accounts', $arrdata);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		} else {
			$this->db->trans_commit();
			$insert_id = $this->db->insert_id();
			return $insert_id;
		}
	}

	function saveUserAdmin($useraccount_id)
	{
		
		$arrdata = array(
			'useraccount_id' 		=> $useraccount_id,
			'role_id' 				=> $this->input->post('role_id'),
			'unit_id' 				=> $this->input->post('unit_id'),
			'username' 				=> $this->input->post('username'),
			'password' 				=> $this->input->post('password'),
			'user_ent' 				=> $this->input->post('user_ent'),
			'date_ent' 				=> $this->input->post('date_ent')		
		);

		$this->db->trans_begin();
		$this->db->insert('user_admin', $arrdata);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		} else {
			$this->db->trans_commit();
			return 1;
		}
	}

	function updateUserAccount()
	{

		$arrdata = array(
			'unit_id' 				=> $this->input->post('unit_id'),
			'id_account_register' 	=> $this->input->post('id_account_register'),
			'name' 					=> $this->input->post('name'),
			'email' 				=> $this->input->post('email'),
			'phone' 				=> $this->input->post('phone'),
			'status' 				=> $this->input->post('status'),
			'user_ent' 				=> $this->input->post('user_ent'),
			'date_ent' 				=> $this->input->post('date_ent')
		);

		$arUpdate = array(
			'id' => $this->input->post('id')
		);

		$this->db->trans_begin();
		$this->db->where($arUpdate);
		$this->db->update('user_accounts', $arrdata);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		} else {
			$this->db->trans_commit();
			return 1;
		}

    }

    function updateUserAdmin()
	{

		$arrdata = array(
			'role_id' 				=> $this->input->post('role_id'),
			'unit_id' 				=> $this->input->post('unit_id'),
			'username' 				=> $this->input->post('username'),
			'password' 				=> $this->input->post('password'),
			'user_ent' 				=> $this->input->post('user_ent'),
			'date_ent' 				=> $this->input->post('date_ent')
		);

		$arUpdate = array(
			'useraccount_id' => $this->input->post('id')
		);

		$this->db->trans_begin();
		$this->db->where($arUpdate);
		$this->db->update('user_admin', $arrdata);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		} else {
			$this->db->trans_commit();
			return 1;
		}

    }

    function deleteUserAccount() 
    {

		$arDelete = array(
			'id' => $this->input->post('id')
		);

		$this->db->trans_begin();
		$this->db->where($arDelete);
		$this->db->delete('user_accounts');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		} else {
			$this->db->trans_commit();
			return 1;
		}

	}

	function deleteUserAdmin() 
    {

		$arDelete = array(
			'useraccount_id' => $this->input->post('id')
		);

		$this->db->trans_begin();
		$this->db->where($arDelete);
		$this->db->delete('user_admin');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		} else {
			$this->db->trans_commit();
			return 1;
		}

	}
}