<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Pengguna extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Models_Master_Pengguna', 'mdl');
	}

	public function index()
	{
		$this->load->view('master_pengguna/index');
	}

	public function add()
	{
		$this->load->view('master_pengguna/add_master_pengguna');
	}
	
	function save() 
	{
		if (isset($_POST['mode'])) {
        	if($_POST['mode'] == "new") {
        		$saveUserAccounts = $this->mdl->saveUserAccount();
        		if($saveUserAccounts != 0) {
        			$saveUserAdmin = $this->mdl->saveUserAdmin($saveUserAccounts);
        			echo json_encode($saveUserAdmin);	
        		} else {
        			echo json_encode(0);
        		}
        	} else {
        		$updateUserAccount = $this->mdl->updateUserAccount();
        		if($updateUserAccount != 0) {
        			$updateUserAdmin = $this->mdl->updateUserAdmin();
        			echo json_encode($updateUserAdmin);	
        		} else {
        			echo json_encode(0);
        		} 
        	}
        } else {
        	echo json_encode(0);
        }
	} 
	public function delete() 
    {
    	if($this->input->post('id') != null) {
    		$deleteUserAccount = $this->mdl->deleteUserAccount();
    		if($deleteUserAccount != 0) {
    			$deleteUserAdmin = $this->mdl->deleteUserAdmin();
				echo json_encode($deleteUserAdmin);
    		} else {
    			echo json_encode(0);
    		}
    		
		} else {
			echo json_encode(0);
		}
    }
}
