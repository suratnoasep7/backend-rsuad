<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('layouts/views_header'); ?>
<div class="content-container">
	<div class="col col-content padding-content">
		<h4 class="bold">DATA PENGGUNA</h4>
		<ul class="sub-menu-container">
			<li class="item"><a href="<?php echo base_url('master/master_pengguna/add') ?>">Tambah Pengguna</a></li>
			<li class="sep">|</li>
			<li class="item active"><a href="<?php echo base_url('master/master_pengguna') ?>">Data Pengguna</a></li>
		</ul>
		<div class="date-info f-green padding-tanggal">
			Jumat, 13 Maret 2020
		</div><br/><br/>
		<div class="flex">
			<div class="f-col-9">
				<div class="head-form-control">Filter Pengguna</div>
					<select class="form-control">
						<option></option>
						<option></option>
					</select>
			</div>
			<div class="b-lock">
				<a href="" class="btn btn-danger rounded pull-right">LOCK</a>
			</div>
		</div>
		<div class="flex">
			<div class="f-col">
				<div class="head-form-control">Data Pengguna</div>
				<table class="table table-green table-bordered">
					<thead>
						<tr>
							<th style="width: 40px;">No</th>
							<th>Nama Pengguna</th>
							<th>Posisi</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td style="width: 300px;">
								<button class='button-action' value="1"></button>
							</td>
						</tr>
					</tbody>
				</table>
					
			</div>
		</div>
	</div>
	<?php $this->load->view('layouts/views_sidebar_master'); ?>
</div>
<?php $this->load->view('layouts/views_footer'); ?>