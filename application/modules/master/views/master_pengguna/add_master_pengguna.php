<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('layouts/views_header'); ?>
<div class="content-container">
	<div class="col col-content m-content padding-content">
		<h4 class="bold">TAMBAH PENGGUNA</h4>
		<ul class="sub-menu-container">
			<li class="item active"><a href="<?php echo base_url('master/master_pengguna/add') ?>">Tambah Pengguna</a></li>
			<li class="sep">|</li>
			<li class="item"><a href="<?php echo base_url('master/master_pengguna') ?>">Data Pengguna</a></li>
		</ul>
		<div class="date-info f-green padding-tanggal">
			Jumat, 13 Maret 2020
		</div><br/><br/>
		<div class="flex">
			<div class="f-col-9">
				<div class="head-form-control">Pilih Karyawan</div>
					<select class="form-control">
						<option></option>
						<option></option>
					</select>
			</div>
			<div class="b-lock">
				<a href="" class="btn btn-danger rounded pull-right">LOCK</a>
			</div>
		</div><br/>
		<div class="flex">
			<div class="f-col">
				<div class="head-form-control">Tambah Data Pengguna</div>
				<form id="pengguna" method="post">
					<div class="label m-input">
						<input type="hidden" name="mode" value="new"> 
						<input type="text" class="form-control" name="name" value="" placeholder="Nama Pengguna" required="">
					</div>
					<div class="label m-input">
						<input type="text" class="form-control" name="username" value="" placeholder="Username" required="">
					</div>
					<div class="label m-input">
						<input type="password" class="form-control" name="password" value="" placeholder="Password" required="">
					</div>
					<div class="label m-input">
						<input type="email" class="form-control" name="email" value="" placeholder="Email" required="">
					</div>
					<div class="label m-input">
						<input type="text" class="form-control input number" name="phone" value="" placeholder="Nomer Tlp" required="">
					</div>
					<div class="label m-input">
						<select class="form-control" name="role_id">
							<option disabled="disabled">Posisi -- CS IT / Petugas Lapangan</option>
							<option value="1">CS IT</option>
							<option value="2">PETUGAS LAPANGAN</option>
						</select>
					</div>
					<div class="label m-input">
						<select class="form-control" name="status">
							<option disabled="disabled">Status -- CS IT / Aktif</option>
							<option value="1">Aktif</option>
							<option value="0">Tidak Aktif</option>
						</select>
					</div>
					<br/>	
					<button type="button" class="btn btn-success btn-lg right shadow btn-tambah-pengguna">SIMPAN</button>
				</form>
			</div>
		</div>
	</div>
	<?php $this->load->view('layouts/views_footer'); ?>
</div>
<?php $this->load->view('layouts/views_footer'); ?>