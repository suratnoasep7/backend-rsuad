<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_Aktifitas extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('laporan_aktifitas');
	}
}
