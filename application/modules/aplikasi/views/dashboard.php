<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('layouts/views_header'); ?>
<div class="content-container">
    <div class="col col-content padding-content">
        <h4 class="bold">DASHBOARD</h4>
        <div class="date-info f-green padding-tanggal">
            Jumat, 13 Maret 2020
        </div><br />
        <div class="flex">
            <div class="f-col-5 f-float-round center pad-lg">
                <div class="head-form-control">Permintaan Pelayanan Hari Ini</div>
                <div class="re-tiket">
                    17
                </div>
                <div class="head-form-control">Tiket</div>

            </div>
            <div class="f-col-5 f-float-round center pad-lg" style="margin-left: 50px;">
                <div class="head-form-control">Permintaan Terselesaikan Hari Ini</div>
                <div class="flex">
                    <div class="f-col-3">
                        <div class="re-open">
                            13
                        </div>
                        <div class="head-form-control">Open</div>
                    </div>
                    <div class="f-col-3">
                        <div class="re-pending">
                            01
                        </div>
                        <div class="head-form-control">Pending</div>
                    </div>
                    <div class="f-col-3">
                        <div class="re-close">
                            17
                        </div>
                        <div class="head-form-control">Close</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex">
            <div class="f-col-9">
                <div class="head-form-control">Aktifitas Harian</div>
            </div>
            <div class="f-col-4">
                <ul class="sub-menu-container">
                    <li class="item"><a href="<?php echo base_url('aplikasi/dashboard/grafik') ?>">Grafik</a></li>
                    <li class="sep">|</li>
                    <li class="item active"><a href="<?php echo base_url('aplikasi/dashboard') ?>">Table</a></li>
                </ul>
            </div>
        </div>
        <div class="flex">
            <div class="f-col box-shadow">
                <select class="form-control w-40">
                    <option>Pilih Ruangan</option>
                    <option></option>
                </select><br>
                <table class="table table-green table-bordered">
                    <col>
                    <colgroup span="2"></colgroup>
                    </col>
                    <tr class="c-success">
                        <th colspan="1" rowspan="2" scope="colgroup">NO</th>
                        <th colspan="1" rowspan="2" scope="colgroup">Tanggal</th>
                        <th colspan="3" scope="colgroup">Permintaan</th>
                        <th colspan="2" scope="colgroup">Asset</th>
                        <th colspan="2" scope="colgroup">Detail Lokasi Asset</th>
                        <th colspan="1" rowspan="2" scope="colgroup"> Status Tiket</th>
                        <th colspan="1" rowspan="2" scope="colgroup"> Status Pelayanan</th>
                    </tr>
                    <tr class="c-success">
                        <th scope="col">Tiket</th>
                        <th scope="col">Waktu</th>
                        <th scope="col">Ruangan</th>
                        <th scope="col">ID Asset</th>
                        <th scope="col">Nama Asset</th>
                        <th scope="col">Area Ruangan</th>
                        <th scope="col">Detail Ruangan</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>13-03-2020</td>
                        <td>1</td>
                        <td>5 Jam</td>
                        <td>Mawar</td>
                        <td>123</td>
                        <td>Test</td>
                        <td>Test</td>
                        <td>Mawar123</td>
                        <td>Pending</td>
                        <td>Menunggu</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>13-03-2020</td>
                        <td>1</td>
                        <td>5 Jam</td>
                        <td>Mawar</td>
                        <td>123</td>
                        <td>Test</td>
                        <td>Test</td>
                        <td>Mawar123</td>
                        <td>Pending</td>
                        <td>Terjadwal</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>13-03-2020</td>
                        <td>1</td>
                        <td>5 Jam</td>
                        <td>Mawar</td>
                        <td>123</td>
                        <td>Test</td>
                        <td>Test</td>
                        <td>Mawar123</td>
                        <td>Pending</td>
                        <td>Proses</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <?php $this->load->view('layouts/views_sidebar'); ?>
</div>
<?php $this->load->view('layouts/views_footer'); ?>