<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('layouts/views_header'); ?>
<div class="content-container">
    <div class="col col-content padding-content">
        <h4 class="bold">DASHBOARD</h4>
        <div class="date-info f-green padding-tanggal">
            Jumat, 13 Maret 2020
        </div><br/>
        <div class="flex">
            <div class="f-col-5 f-float-round center pad-lg">
                <div class="head-form-control">Permintaan Pelayanan Hari Ini</div>
                <div class="re-tiket">
                    17
                </div>
                <div class="head-form-control">Tiket</div>
    
            </div>
            <div class="f-col-5 f-float-round center pad-lg" style="margin-left: 50px;">
                <div class="head-form-control">Permintaan Terselesaikan Hari Ini</div>
                    <div class="flex">
                        <div class="f-col-3">
                            <div class="re-open">
                                13
                            </div>
                            <div class="head-form-control">Open</div>
                        </div>
                        <div class="f-col-3">
                            <div class="re-pending">
                                01
                            </div>
                            <div class="head-form-control">Pending</div>
                        </div>
                        <div class="f-col-3">
                            <div class="re-close">
                                17
                            </div>
                            <div class="head-form-control">Close</div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="flex">
            <div class="f-col-9">
                <div class="head-form-control">Aktifitas Harian</div>
            </div>
            <div class="f-col-4">
                <ul class="sub-menu-container">
                    <li class="item active"><a href="<?php echo base_url('aplikasi/dashboard/grafik') ?>">Grafik</a></li>
                    <li class="sep">|</li>
                    <li class="item"><a href="<?php echo base_url('aplikasi/dashboard'); ?>">Table</a></li>
                </ul>
            </div>
        </div>
        <div class="flex">
            <div class="f-col box-shadow">
                <select class="form-control w-40">
                    <option>Pilih Ruangan</option>
                    <option></option>
                </select><br>
                <div id="container-chart-line"></div>
            </div>
        </div>
    </div>
    <?php $this->load->view('layouts/views_sidebar'); ?>
</div>
<?php $this->load->view('layouts/views_footer'); ?>