<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('layouts/views_header'); ?>
<div class="content-container">
	<div class="col col-content padding-content">
		<h4 class="bold">LAPORAN AKTIVITAS</h4>
		<div class="date-info f-green padding-tanggal">
			Jumat, 13 Maret 2020
		</div><br />
		<div class="f-float-round pad-sm">
			<div class="flex">
				<div class="f-col-8">
					<div class="head-form-control">Filter</div>
					<select class="form-control">
						<option>Filter Ruangan</option>
						<option></option>
					</select>
				</div>
			</div>
			<div class="flex">
				<div class="f-col-4">
					<select class="form-control">
						<option>Bulan</option>
						<option></option>
					</select>
				</div>
				<div class="f-col-4">
					<select class="form-control">
						<option>Tahun</option>
						<option></option>
					</select>
				</div>
			</div>
			<div class="flex">
				<div class="f-col-4">
					<a href="" class="btn c-success rounded pull-right" style="width: 100px;">CARI</a>
				</div>
			</div>
		</div><br>
		<div class="flex">
			<div class="f-col">
				<div class="head-form-control">Laporan Aktifitas Bulan Desember 2019</div>
				<table class="table table-green table-bordered">
					<col>
					<colgroup span="2"></colgroup>
					</col>
					<tr class="c-success">
						<th colspan="1" rowspan="2" scope="colgroup">NO</th>
						<th colspan="1" rowspan="2" scope="colgroup">Tanggal</th>
						<th colspan="3" scope="colgroup">Permintaan</th>
						<th colspan="2" scope="colgroup">Asset</th>
						<th colspan="2" scope="colgroup">Lokasi Asset</th>
						<th colspan="1" rowspan="2" scope="colgroup"> Status Tiket</th>
					</tr>
					<tr class="c-success">
						<th scope="col">Tiket</th>
						<th scope="col">Waktu</th>
						<th scope="col">Ruangan</th>
						<th scope="col">ID Asset</th>
						<th scope="col">Nama Asset</th>
						<th scope="col">Area Ruangan</th>
						<th scope="col">Detail Ruangan</th>
					</tr>
					<tr>
						<td>1</td>
						<td>13-03-2020</td>
						<td>1</td>
						<td>5 Jam</td>
						<td>Mawar</td>
						<td>123</td>
						<td>Test</td>
						<td>Test</td>
						<td>Mawar123</td>
						<td>Pending</td>
					</tr>
					<tr>
						<td>2</td>
						<td>13-03-2020</td>
						<td>1</td>
						<td>5 Jam</td>
						<td>Mawar</td>
						<td>123</td>
						<td>Test</td>
						<td>Test</td>
						<td>Mawar123</td>
						<td>Pending</td>
					</tr>
					<tr>
						<td>3</td>
						<td>13-03-2020</td>
						<td>1</td>
						<td>5 Jam</td>
						<td>Mawar</td>
						<td>123</td>
						<td>Test</td>
						<td>Test</td>
						<td>Mawar123</td>
						<td>Pending</td>
					</tr>
				</table>

			</div>
		</div>
	</div>
	<?php $this->load->view('layouts/views_sidebar'); ?>
</div>
<?php $this->load->view('layouts/views_footer'); ?>